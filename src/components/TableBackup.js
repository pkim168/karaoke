import React, { Component } from 'react';
import '../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
// import '../../node_modules/react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
const process = window.require('process');
// import BootstrapTable from 'react-bootstrap-table-next';

class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      json: this.props.json,
      table: this.props.table
    };
    this.addSong = this.addSong.bind(this);
    this.removeSong = this.removeSong.bind(this);
  }

  addSong(song) {
    this.props.addSong(song);
  }

  removeSong(song) {
    this.props.removeSong(song);
  }

  render() {
    var data = [];

    var tableData = this.props.json;
    data = Object.values(tableData);

    let header = (
      <tr className="d-flex" key={this.props.json + "head"}>
        <th className="col-1" scope="col" key={this.props.table + 'Code'}>Code</th>
        <th className="col" scope="col" key={this.props.table + 'Title'}>Title</th>
        <th className="col" scope="col" key={this.props.table + 'Artist'}>Artist</th>
        <th className="col" scope="col" key={this.props.table + 'Album'}>Album</th>
        <th className="col-2" scope="col" key={this.props.table + 'Button'}></th>
      </tr>
    );

    let rows = data.map(function(song, i) {
      let time = process.hrtime.bigint();
      let key = song.code + time;
      return (
        <tr className="d-flex" key={"row" + i + key}>
          <th className="col-1" scope="row" key={key}>{song.code}</th>
          <td className="col" key={key + "title"}>{song.title}</td>
          <td className="col" key={key + "artist"}>{song.artist}</td>
          <td className="col" key={key + "album"}>{song.album}</td>
          {this.props.table === 'library' &&
            <td className="col-2" key={"button"+song.code + time}><button className="btn btn-outline-dark" key={key} onClick={() => this.props.addSong(song)}>Add to Queue</button></td>}
          {this.props.table === 'queue' &&
            <td className="col-2" key={"button"+song.code + time}><button className="btn btn-outline-dark" key={key} onClick={() => this.props.removeSong(i)}>Remove from Queue</button></td>}
        </tr>
      );
    }, this);

    const table = (
      <table className="table table-fixed">
        <thead>
          {header}
        </thead>
        <tbody className="scroll">
          {rows}
        </tbody>
      </table>
    );

    return (
      <div>
        {table}
      </div>
    );
  }
}

export default Table;
