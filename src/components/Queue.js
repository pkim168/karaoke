import React, { Component } from 'react';
import $ from 'jquery';
import '../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import QueueTable from './QueueTable.js';

class Queue extends Component {
  constructor(props) {
    super(props);
    this.state = {
      queue: this.props.queue,
      open: false
    };
  }

  componentDidMount(){
    // console.log("Queue Mount");
    // console.log("Length: " + this.props.queue.length);
    // console.log("Playing: " + !this.props.playing);
    $('.dropdown').on({
      "shown.bs.dropdown": function() { this.closable = false; },
      "hide.bs.dropdown":  function(e) {
        if (e.clickEvent) {
          e.preventDefault();
        }
      }
    });
  }

  shouldComponentUpdate() {
    return true;
  }

  componentDidUpdate(){
    // console.log("Queue Update");
    // console.log("Length: " + this.props.queue.length);
    // console.log("Playing: " + !this.props.playing);
    if (this.props.queue.length && !this.props.playing && !this.props.stopped) {
      // console.log("Queue Update Conditional");
      this.props.onNewSong();
    }
  }

  render() {
    // console.log(this.props.queue);
    // console.log(this.props.flag);
    return (
      <div className="dropdown" >
        <a className="nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Queue </a>
        <div className="dropdown-menu" id="test" aria-labelledby="dropdownMenuButton">
          <QueueTable table={'queue'} json={this.props.queue} removeSong={this.props.removeSong} flag={this.props.flag}/>
        </div>
      </div>
    );
  }
}

export default Queue;
