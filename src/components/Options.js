import React, { Component } from 'react';
import '../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import * as mm from 'music-metadata-browser';
var Promise = window.require('bluebird');
const path = window.require('path');
const fs = Promise.promisifyAll(window.require('fs'));
var glob = window.require('glob');

class Options extends Component {
  constructor(props) {
    super(props);
    this.state = { };
    this.reloadLibrary = this.reloadLibrary.bind(this);
    this.readFiles = this.readFiles.bind(this);
  }

  shouldComponentUpdate(nextProps) {
    return false;
  }

  async readFiles(songs, onFileContent, onError) {
    var promises = [];
    var paths = [];
    songs.forEach(function(song) {
      // console.log("readFiles for loop");
      var content = fs.readFileSync(path.resolve(song));
      var blob = new Blob([content]);
      promises.push(mm.parseBlob(blob).then(
        function(promise) {
          return  {
            path: song,
            data: promise
          };
        }
      ));
      paths.push(song);
    });
    return Promise.all(promises);
  }

  reloadLibrary() {
    const directoryPath = './src/assets/songs';
    const songs = glob.sync(directoryPath + '/**/*.mp3');
    var data = {};
    // console.log("Before readFiles");
    this.readFiles(songs)
    .then(result => {
      var code = 0;
      result.forEach(function(song) {
        var songData = {};
        var metadata = song.data;
        var path = song.path;
        var title = metadata.common.title;
        var artist = metadata.common.artist;
        var album = metadata.common.album;
        var bpm = metadata.common.bpm;
        var key = metadata.common.key;
        var length = metadata.format.duration;
        if (title)
          songData.title = title;
        else
          songData.title = 'No Title';
        if (artist)
          songData.artist = artist;
        else
          songData.artist = 'No Artist';
        if (album)
          songData.album = album;
        else
          songData.album = 'No Album';
        if (bpm)
          songData.bpm = bpm;
        else
          songData.bpm = 'No BPM';
        if (key)
          songData.key = key;
        else
          songData.key = 'No Key';
        if (length)
          songData.length = length;
        else
          songData.length = 'No Length';
        code++;
        songData.code = (code.toString()).padStart(4, '0');
        songData.audioPath = path;
        songData.videoPath = path.substring(0, path.length-4).concat('.mp4');
        songData.lyricPath = path.substring(0, path.length-4).concat('.ass');
        data[songData.code] = songData;
      });
      // console.log(data);
      const json = JSON.stringify(data, null, 2);

      // console.log(json);
      fs.writeFileSync('./src/assets/library.json', json, err => {
        if (err) {
          console.log('Error: ' + err);
          return;
        }
        // console.log('JSON saved');
        this.setState({library: json});
        // console.log(this.state.library);
      });
    }, function(err) {
      // console.log("readFiles failure");
      console.log('Error: ' + err);
      return;
    });
  }

  render() {
    return (
      <div>
        <a className="nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Options </a>
        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <a className="dropdown-item" onClick={this.reloadLibrary}> Reload Library </a>
        </div>
      </div>
    );
  }
}

export default Options;
