import React, { Component } from 'react';
import '../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import ReactPlayer from 'react-player';


class Song extends Component {
  constructor(props) {
    super(props);
    this.state = {
      song: this.props.song,
      playing: this.props.playing
    };
  }

  render() {
    // console.log(this.props.song);
    // console.log(this.props.playing);
    return (
      <div className='player-wrapper'>
        <ReactPlayer
          className='react-player'
          key={this.props.flag}
          playing={this.props.playing}
          url={this.props.song}
          width='100%'
          height='100%'
          onEnded={this.props.onSongEnd}
        />
      </div>
    );
  }
}

export default Song;
