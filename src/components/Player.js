import React, { Component } from 'react';
import '../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Video from './Video.js';
import Song from './Song.js';
import Lyrics from './Lyrics.js';


class Player extends Component {
  constructor(props) {
    super(props);
    this.state = {
      song: this.props.song,
      lyrics: this.props.lyrics,
      video: this.props.video
    };
    this.test = this.test.bind(this);
  }

  test() {
    alert('test');
  }

  // Read all lines from lyrics and put into array(Length of time, lines to show)
  // iterate through array
  // show new line
  // use length of time as timeout between each iteration

  shouldComponentUpdate(nextProps) {
    return true;
  }

  render() {

    return (
      <div className="Video">
        <div className="video-background-holder">
          <div className="video-background-overlay"></div>
          <Video video={this.props.video} flag={this.props.flag} loop={this.props.loop}/>
          <div className="video-background-content container w-100 h-100">
            <div className="d-flex w-100 h-100 text-center align-items-end">
              <div className="w-100 h-25 text-white">
                {/*<Lyrics lyrics={this.props.lyrics} flag={this.props.flag} playing={this.props.playing}/>*/}
              </div>
            </div>
            {/*<Song song={this.props.song} playing={this.props.playing} flag={this.props.flag} onSongEnd={this.props.onSongEnd}/>*/}
          </div>
        </div>
      </div>
    );
  }
}

export default Player;
