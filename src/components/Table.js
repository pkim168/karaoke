import React, { Component } from 'react';
import '../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { useTable, useFilters, useGlobalFilter, useAsyncDebounce } from 'react-table'
import matchSorter from 'match-sorter'

// import '../../node_modules/react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
const process = window.require('process');


function addSong(props, song) {
  props.addSong(song);
}

function removeSong(props, song) {
  props.removeSong(song);
}

function GlobalFilter({
  preGlobalFilteredRows,
  globalFilter,
  setGlobalFilter,
}) {
  const count = preGlobalFilteredRows.length
  const [value, setValue] = React.useState(globalFilter)
  const onChange = useAsyncDebounce(value => {
    setGlobalFilter(value || undefined)
  }, 200)

  return (
    <div className="input-group mb-3">
      <div className="input-group-prepend">
        <span className="input-group-text" id="basic-addon1">Search:</span>
      </div>
      <input type="text" className="form-control" placeholder="" aria-label="Username" aria-describedby="basic-addon1" value={value || ""}
        onChange={e => {
          setValue(e.target.value);
          onChange(e.target.value);
        }}
      />
    </div>
  )
}

function DefaultColumnFilter({
  column: { filterValue, preFilteredRows, setFilter },
}) {
  const count = preFilteredRows.length

  return (
    <input
      value={filterValue || ''}
      onChange={e => {
        setFilter(e.target.value || undefined) // Set undefined to remove the filter entirely
      }}
      placeholder={`Search ${count} records...`}
    />
  )
}

function fuzzyTextFilterFn(rows, id, filterValue) {
  return matchSorter(rows, filterValue, { keys: [row => row.values[id]] })
}

const TableComponent = (props) => {
  const defaultColumn = React.useMemo(
    () => ({
      // Let's set up our default Filter UI
      Filter: DefaultColumnFilter,
    }),
    []
  )

  const filterTypes = React.useMemo(
    () => ({
      // Add a new fuzzyTextFilterFn filter type.
      fuzzyText: fuzzyTextFilterFn,
      // Or, override the default text filter to use
      // "startWith"
      text: (rows, id, filterValue) => {
        return rows.filter(row => {
          const rowValue = row.values[id]
          return rowValue !== undefined
            ? String(rowValue)
                .toLowerCase()
                .startsWith(String(filterValue).toLowerCase())
            : true
        })
      },
    }),
    []
  );

  const columns = React.useMemo(
      () => [
          {
              Header: 'Code',
              accessor: 'code', // accessor is the "key" in the data
              show: false
          },
          {
              Header: 'Title',
              accessor: 'title',
          },
          {
              Header: 'Artist',
              accessor: 'artist',
          },
          {
              Header: 'Album',
              accessor: 'album',
          },
          {
              Header: '',
              accessor: 'button',
              Cell: ({ row: { original } }) => {
                if (props.table === 'library') {
                  return (<button className="btn btn-outline-dark" onClick={() => addSong(props, original.song)}>Add to Queue</button>)
                }
                else if (props.table === 'queue') {
                 return (<button className="btn btn-outline-dark" onClick={() => removeSong(props, original.index)}>Remove from Queue</button>)
                }
              },
          },
      ],[]
  );

  var tableData = Object.values(props.json);
  const data = React.useMemo(
    () => tableData.map(function(song, i) {
      let time = process.hrtime.bigint();
      let key = song.code + time;
      return (
        {
          code: song.code,
          title: song.title,
          artist: song.artist,
          album: song.album,
          song: song,
          index: i
        }
      );
    }), []
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state,
    visibleColumns,
    preGlobalFilteredRows,
    setGlobalFilter,
  } = useTable(
    {
      columns,
      data,
      defaultColumn, // Be sure to pass the defaultColumn option
      filterTypes,
    },
    useFilters, // useFilters!
    useGlobalFilter // useGlobalFilter!
  )

  const table = (
    <>
      {props.table === "library" && <GlobalFilter
        preGlobalFilteredRows={preGlobalFilteredRows}
        globalFilter={state.globalFilter}
        setGlobalFilter={setGlobalFilter}
      />}
      <table key={props.flag} className="table table-hover table-sm" {...getTableProps()}>
        <thead>
          {headerGroups.map(headerGroup => (
            <tr className="d-flex" {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map(column => (
                <th className="col" {...column.getHeaderProps()}>
                  {column.render('Header')}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody className="scroll" {...getTableBodyProps()}>
          {rows.map((row, i) => {
            prepareRow(row)
            return (
              <tr className="d-flex" {...row.getRowProps()}>
                {row.cells.map(cell => {
                  return <td className="col" {...cell.getCellProps()}>{cell.render('Cell')}</td>
                })}
              </tr>
            )
          })}
        </tbody>
      </table>
    </>
  );
  return table;
}

class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      json: this.props.json,
      table: this.props.table
    };
  }

  render() {
    const table = (
      <TableComponent key={this.props.flag} {...this.props}/>
    )
    // console.log(this.props.json);
    return (
      <div className="table-fixed">
        {table}
      </div>
    );
  }
}

export default Table;
