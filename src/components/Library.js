import React, { Component } from 'react';
import '../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Table from './Table.js';

class Library extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  shouldComponentUpdate(nextProps) {
    const library = this.props.library !== nextProps.library;
    return library;
  }

  render() {
    return (
      <div className="dropdown">
        <a className="nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Add Song </a>
        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <Table table={'library'} json={this.props.library} addSong={this.props.addSong}/>
        </div>
      </div>
    );
  }
}

export default Library;
