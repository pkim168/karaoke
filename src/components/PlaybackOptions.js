import React, { Component } from 'react';
import '../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import $ from 'jquery';

class PlaybackOptions extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  shouldComponentUpdate(nextProps) {
    return false;
  }

  componentDidMount(){
    $('.dropdown').on({
      "shown.bs.dropdown": function() { this.closable = false; },
      "hide.bs.dropdown":  function(e) {
        if (e.clickEvent) {
          e.preventDefault();
        }
      }
    });
  }

  render() {
    return (
      <div className="dropdown" >
        <a className="nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Playback Controls </a>
        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <a className="dropdown-item" onClick={this.props.skipSong}> Skip </a>
          <a className="dropdown-item" onClick={this.props.stopSong}> Stop </a>
          <a className="dropdown-item" onClick={this.props.playSong}> Play </a>
        </div>
      </div>
    );
  }
}

export default PlaybackOptions;
