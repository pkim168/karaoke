import React, { Component } from 'react';
import '../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';


class Video extends Component {
  constructor(props) {
    super(props);
    this.state = {
      video: this.props.video,
      muted: this.props.playing
    };
  }

  shouldComponentUpdate() {
    return true;
  }

  render() {
    return (
      <video key={this.props.flag} playsInline="playsinline" autoPlay loop={this.props.loop}>
        <source src={this.props.video} type="video/mp4" />
      </video>
    );
  }
}

export default Video;
