import React, { Component } from 'react';
import '../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Queue from './Queue.js';
import Library from './Library.js';
import Options from './Options.js';
import PlaybackOptions from './PlaybackOptions.js';

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   return false;
  // }

  // buildComponentMap() {
  //   return {
  //     "Queue": <Queue />,
  //     "Library": <Library/>
  //   }
  // }

  render() {
    return (
      <div className="Navbar">
        <nav className="navbar navbar-expand-lg navbar-light bg-white py-3 shadow-sm fixed-top">
            <button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" className="navbar-toggler"><span className="navbar-toggler-icon"></span></button>
            <div id="navbarSupportedContent" className="collapse navbar-collapse">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <Queue queue={this.props.queue} onNewSong={this.props.onNewSong} removeSong={this.props.removeSong} playing={this.props.playing} stopped={this.props.stopped} flag={this.props.flag}/>
                </li>
                <li className="nav-item">
                  <Library library={this.props.library} addSong={this.props.addSong}/>
                </li>
                <li className="nav-item">
                  <PlaybackOptions skipSong={this.props.skipSong} stopSong={this.props.stopSong} playSong={this.props.playSong}/>
                </li>
                <li className="nav-item">
                  <Options />
                </li>
              </ul>
            </div>
        </nav>
      </div>
    );
  }
}

export default Navbar;
