import React, { Component } from 'react';
import './App.css';
import '../node_modules/bootstrap/dist/js/bootstrap.min.js';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Navbar from './components/Navbar.js';
import Player from './components/Player.js';
import library from './assets/library.json';
import video from './assets/sample.mp4';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      apiResponse: "" ,
      library: library,
      queue: [],
      playing: false,
      stopped: false,
      song: '',
      video: video,
      flag: true,
      lyrics: '',
      loop: true
    };
    this.addSong = this.addSong.bind(this);
    this.removeSong = this.removeSong.bind(this);
    this.onNewSong = this.onNewSong.bind(this);
    this.skipSong = this.skipSong.bind(this);
    this.stopSong = this.stopSong.bind(this);
    this.playSong = this.playSong.bind(this);
    this.onSongEnd = this.onSongEnd.bind(this);
  }

  addSong(song) {
    // Add song to queue
    console.log('Add Song');
    this.setState(prevState => ({
      queue: [...prevState.queue, song]
    }));
  }

  removeSong(index) {
    // Remove song from queue
    console.log('Remove Song');
    let queue = this.state.queue;
    queue.splice(index, 1);
    console.log("");
    this.setState({
      queue: queue
    });
  }

  onSongEnd() {
    console.log('On Song End');
    if (this.state.queue.length) {
      this.onNewSong()
    }
    else{
      this.setState({
        song: '',
        video: video,
        lyrics: '',
        playing: false,
        stopped: false,
        flag: !this.state.flag,
        loop: true
      })
    }
  }

  onNewSong() {
    console.log("On New Song");
    console.log("Flag: " + this.state.flag);
    // Play new song
    // Remove song from queue
    this.setState({
      queue: this.state.queue.slice(1),
      song: require('.' + this.state.queue[0].audioPath.substring(5)),
      video: require('.' + this.state.queue[0].videoPath.substring(5)),
      lyrics: require('.' + this.state.queue[0].lyricPath.substring(5)),
      playing: true,
      stopped: false,
      flag: !this.state.flag,
      loop: false
    })
  }

  skipSong() {
    // Play new song
    // Remove song from queue
    if(this.state.queue.length && !this.state.stopped){
      console.log('Skip Song');
      console.log("Flag: " + this.state.flag);
      this.setState({
        queue: this.state.queue.slice(1),
        song: require('.' + this.state.queue[0].audioPath.substring(5)),
        video: require('.' + this.state.queue[0].videoPath.substring(5)),
        lyrics: require('.' + this.state.queue[0].lyricPath.substring(5)),
        playing: true,
        stopped: false,
        flag: !this.state.flag,
        loop: false
      })
    }
    else if (!this.state.queue.length && !this.state.stopped && this.state.playing) {
      console.log('Skip Song');
      console.log("Flag: " + this.state.flag);
      this.setState({
        queue: this.state.queue.slice(1),
        song: '',
        video: video,
        lyrics: '',
        playing: false,
        stopped: false,
        flag: !this.state.flag,
        loop: true
      })
    }
  }

  stopSong() {
    if(this.state.playing && !this.state.stopped) {
      console.log('Stop Song');
      console.log("Flag: " + this.state.flag);
      // Stop song
      let stop;
      this.state.queue.length ? stop = true : stop = false;
      this.setState({
        song: '',
        video: video,
        lyrics: '',
        playing: false,
        stopped: stop,
        flag: !this.state.flag,
        loop: true
      })
    }
  }

  playSong() {
    if (!this.state.playing && this.state.stopped && this.state.queue.length) {
      console.log('Play Song');
      console.log("Flag: " + this.state.flag);
      // Play new song
      // Remove song from queue
      this.setState({
        queue: this.state.queue.slice(1),
        song: require('.' + this.state.queue[0].audioPath.substring(5)),
        video: require('.' + this.state.queue[0].videoPath.substring(5)),
        lyrics: require('.' + this.state.queue[0].lyricPath.substring(5)),
        playing: true,
        stopped: false,
        flag: !this.state.flag,
        loop: false
      })
    }
  }

  shouldComponentUpdate(nextProps) {
    return true;
  }

  callAPI() {
    fetch("http://localhost:9000/testAPI")
      .then(res => res.text())
      .then(res => this.setState({ apiResponse: res }))
      .catch(err => err);
  }

  componentDidMount() {
    // this.callAPI();
  }

  render() {
    return (
      <div className="App">
        <Navbar queue={this.state.queue} library={this.state.library} onNewSong={this.onNewSong} addSong={this.addSong} removeSong={this.removeSong} playing={this.state.playing} stopped={this.state.stopped} skipSong={this.skipSong} stopSong={this.stopSong} playSong={this.playSong} flag={this.state.flag}/>
        <Player song={this.state.song} flag={this.state.flag} playing={this.state.playing} lyrics={this.state.lyrics} video={this.state.video} loop={this.state.loop} onSongEnd={this.onSongEnd}/>
      </div>
    );
  }
}

export default App;
